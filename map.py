#!/usr/bin/env python3
import math
import matplotlib.pyplot as plt
import enum
import random
import numpy as np
from scipy.sparse.csgraph import dijkstra, minimum_spanning_tree
from pyglet.gl import *

def add2d(a, b):
    return (a[0]+b[0], a[1]+b[1])

class TileType(enum.Enum):
    ROAD = "ROAD"
    FEATURE = "FEATURE"
    DOOR = "DOOR"
    INACCESSIBLE = "INACCESSIBLE" 

class Hexagon():
    def __init__(self, pos):
        self.pos = pos
        self.neighbours = []
        self.tile = TileType.INACCESSIBLE

    def cartesian(self):
       left = (0, 1)
       fw = (math.sin(math.pi/3.0), math.cos(math.pi/3.0))
  
       return (self.pos[0]*fw[0] + self.pos[1]*left[0], 
               self.pos[0]*fw[1] + self.pos[1]*left[1])

    def distance(a,b):
        az = a.pos[0]+a.pos[1]
        bz = b.pos[0]+b.pos[1]
        return (abs(a.pos[0]-b.pos[0]) + abs(a.pos[1]-b.pos[1]) + abs(az-bz))/2

    def generateTriangles(self):
        c = self.cartesian()
        s= 0.5/math.cos(math.pi/6)
        def offset(theta):
            return (s*math.cos(theta), s*math.sin(theta))         
        for theta in np.linspace(0, 2*math.pi, 7):
            yield [c, add2d(c,offset(theta)), add2d(c, offset(theta+math.pi/3))]

    def color(self):
        if self.tile == TileType.ROAD:
            return (1.0, 1.0, 0.0)

        if self.tile == TileType.FEATURE:
            return (0.0, 0.0, 1.0)

        if self.tile == TileType.INACCESSIBLE:
            return (0.0, 1.0, 0.0)

        if self.tile == TileType.DOOR:
            return (1.0, 0.0, 0.0)

class Map():
    def __init__(self, size, features, feature_radius):
        self.m = {h.pos:h for h in self._generate_hex_grid(size)}
        self._add_neighbours_to_hex_map()
        self._make_features(features, feature_radius)
        self._unlink_features()
        self._build_roads()

    def _generate_hex_grid(self, length):
        for q in range(-length+1, length):
            r1 = max(-length, -q - length)
            r2 = min(length, -q + length)
            for r in range(r1+1, r2):
                yield Hexagon((q, r))

    def _add_neighbours_to_hex_map(self):
        directions = [(-1, 0), (1, 0), (0, 1), (0, -1), (1, -1), (-1, 1)]
        for k, h in self.m.items():
            for d in directions:
                neighbour_pos = (d[0]+h.pos[0], d[1]+h.pos[1])
                if neighbour_pos in self.m:
                    h.neighbours.append(self.m[neighbour_pos])

    def plot(self):
        for t, c in zip(TileType, ["yo", "bo", "ro", "go"]):
            filtered = [v for k, v in self.m.items() if v.tile == t]
            if len(filtered) > 0:
                x, y = zip(*[h.cartesian() for h in filtered])
                plt.plot(x, y, c)
        plt.show()

    def _make_features(self, count, radius):
        features = []
        def mark_feature(tile, depth):
            if(depth > 0):
                tile.tile = TileType.FEATURE
                for t in tile.neighbours:
                    yield from mark_feature(t, depth-1)
            yield tile
        def new_feature():
            p = random.choice(list(self.m.keys()))
            t = self.m[p]
            if all((f.distance(t)>3*radius for f in features)):
                features.append(t)
                surrounding_tiles = list(mark_feature(t, radius))
                door_candidates = [t for t in surrounding_tiles if not t.tile is TileType.FEATURE]
                door = random.choice(door_candidates)
                door.tile = TileType.DOOR
            else:
                new_feature()
        for i in range(0, count):
            new_feature()

    def _unlink_features(self):
        for k, v in self.m.items():
            if v.tile is TileType.FEATURE:
                v.neighbours = []
            else:
                v.neighbours = [t for t in v.neighbours if not t.tile is TileType.FEATURE]

    def _build_roads(self):
        doors = [v for k, v in self.m.items() if v.tile is TileType.DOOR] 
        items = list(self.m.items())
        indices = {kv[1].pos:i for i, kv in enumerate(items)}
        adjacencyMatrix = np.zeros((len(self.m), len(self.m)))
        for p, t in items:
            for n in t.neighbours:
                adjacencyMatrix[indices[p]][indices[n.pos]] = 1

        shortestDistances, paths = dijkstra(adjacencyMatrix, return_predecessors=True)

        roadIndices = [indices[d.pos] for d in doors]
        roadMatrix = shortestDistances[roadIndices,:][:,roadIndices]
        mst = minimum_spanning_tree(roadMatrix)

        for start, end in zip(*mst.nonzero()):
            startIndex = roadIndices[start]
            endIndex = roadIndices[end]
            def fill_path(startIndex, endIndex):
                i = paths[endIndex][startIndex]
                if(i != endIndex):
                    tile = items[i][1]
                    if not tile.tile is TileType.DOOR:
                        tile.tile = TileType.ROAD
                    fill_path(i, endIndex)
            fill_path(startIndex, endIndex)

    def generateTriangles(self):
        for k, v in self.m.items():
             for t in v.generateTriangles():
                 yield (v, t)
    def render(self):
        glBegin(GL_TRIANGLES)
        s=10.0
        for h, t in self.generateTriangles():
            glColor3f(*h.color())
            glVertex2f(250+s*t[0][0], 225+s*t[0][1])
            glVertex2f(250+s*t[1][0], 225+s*t[1][1])
            glVertex2f(250+s*t[2][0], 225+s*t[2][1])
        glEnd()

if __name__ == "__main__":
    m = Map(25, 15, 3)
    m.plot()
