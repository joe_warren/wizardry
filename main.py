#!/usr/bin/env python3
import pyglet
from pyglet.gl import *
import map

if __name__=="__main__": 
    window = pyglet.window.Window()
    context = window.context

    m = map.Map(25, 15, 3)

    @window.event
    def on_draw():
      glClear(GL_COLOR_BUFFER_BIT)
      glLoadIdentity()
      m.render()

    pyglet.app.run()
